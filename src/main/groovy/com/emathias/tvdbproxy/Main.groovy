package com.emathias.tvdbproxy

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@ComponentScan("com.emathias.tvdbproxy")
class Main {

    static main(args) {
        new SpringApplication(Main).run(args);
    }
}

package com.emathias.tvdbproxy.controller

import com.emathias.tvdbproxy.dto.EpisodeData
import com.emathias.tvdbproxy.dto.RatingData
import com.emathias.tvdbproxy.dto.SeriesData
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate

@RestController
@RequestMapping("/api")
class ApiController {

    @RequestMapping("/GetSeries.php")
    public ResponseEntity<?> getSeries(
            @RequestParam String seriesname,
            @RequestParam(required = false, defaultValue = "en") String language) {

        RestTemplate template = new RestTemplate();

        return template.getForEntity(
                "http://thetvdb.com/api/GetSeries.php?seriesname={seriesname}&language={language}",
                SeriesData, seriesname, language);
    }

    @RequestMapping("/GetSeriesByRemoteID.php")
    public ResponseEntity<?> getSeriesByRemoteId(
            @RequestParam(required = false, defaultValue = "") String imdbid,
            @RequestParam(required = false, defaultValue = "") String zap2it,
            @RequestParam(required = false, defaultValue = "en") String language) {

        RestTemplate template = new RestTemplate();

        return template.getForEntity(
                "http://thetvdb.com/api/GetSeriesByRemoteID.php?imdbid={imdbid}&zap2it={zap2it}&language={language}",
                SeriesData, imdbid, zap2it, language);
    }

    @RequestMapping("/GetEpisodeByAirDate.php")
    public ResponseEntity<?> getEpisodeByAirDate(
            @RequestParam String apikey,
            @RequestParam String seriesid,
            @RequestParam String airdate,
            @RequestParam(required = false, defaultValue = "en") String language) {

        RestTemplate template = new RestTemplate();

        return template.getForEntity(
                "http://thetvdb.com/api/GetEpisodeByAirDate.php?apikey={apikey}&seriesid={seriesid}&airdate={airdate}&language={language}",
                EpisodeData, apikey, seriesid, airdate, language);
    }

    @RequestMapping("/GetRatingsForUser.php")
    public ResponseEntity<?> getRatingsForUser(
            @RequestParam String apikey,
            @RequestParam String accountid,
            @RequestParam(required = false) String seriesid) {

        RestTemplate template = new RestTemplate();

        return template.getForEntity(
                "http://thetvdb.com/api/GetRatingsForUser.php?apikey={apikey}&accountid={accountid}&seriesid={seriesid}",
                RatingData, apikey, accountid, seriesid);
    }
}

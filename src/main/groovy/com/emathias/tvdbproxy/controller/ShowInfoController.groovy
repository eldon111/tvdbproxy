package com.emathias.tvdbproxy.controller

import com.emathias.tvdbproxy.mapping.EpisodeDataSource
import com.emathias.tvdbproxy.mapping.EpisodeDataSourceFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api")
class ShowInfoController extends BaseController {

//    private final EpisodeDataSourceFactory episodeDataSourceFactory
//
//    @Autowired
//    ShowInfoController(EpisodeDataSourceFactory episodeDataSourceFactory) {
//        this.episodeDataSourceFactory = episodeDataSourceFactory
//    }

    @RequestMapping("/{apiKey}/series/{seriesId}/all/{lang}.xml")
    public ResponseEntity<?> getSeriesInfo(
            @PathVariable String apiKey,
            @PathVariable Long seriesId,
            @PathVariable String lang) {

        EpisodeDataSource episodeDataSource =
                EpisodeDataSourceFactory.getTvdbEpisodeDataSource(apiKey, lang)

        return ResponseEntity.ok(episodeDataSource.loadDataForSeries(seriesId))
    }
}

package com.emathias.tvdbproxy.domain

class EpisodeMapping {

    EpisodeMapping(int srcSeason, int srcEpisode, int destSeason, int destEpisode) {
        this.srcSeason = srcSeason
        this.srcEpisode = srcEpisode
        this.destSeason = destSeason
        this.destEpisode = destEpisode
    }

    int srcSeason
    int srcEpisode
    int destSeason
    int destEpisode
}

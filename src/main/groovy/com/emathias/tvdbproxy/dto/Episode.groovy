package com.emathias.tvdbproxy.dto

import groovy.transform.AutoClone

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement

@AutoClone
@XmlAccessorType(XmlAccessType.NONE)
class Episode {

    @XmlElement
    long id

    @XmlElement
    int Combined_episodenumber

    @XmlElement
    int Combined_season

    @XmlElement
    int DVD_chapter

    @XmlElement
    int DVD_discid

    @XmlElement
    int DVD_episodenumber

    @XmlElement
    int DVD_Season

    @XmlElement
    String Director

    @XmlElement
    boolean EpImgFlag

    @XmlElement
    String EpisodeName

    @XmlElement
    int EpisodeNumber

    @XmlElement
    String FirstAired

    @XmlElement
    String GuestStars

    @XmlElement
    String IMDB_ID

    @XmlElement
    String Language

    @XmlElement
    String Overview

    @XmlElement
    String ProductionCode

    @XmlElement
    String Rating

    @XmlElement
    int RatingCount

    @XmlElement
    int SeasonNumber

    @XmlElement
    String Writer

    @XmlElement
    long absolute_number

    @XmlElement
    String filename

    @XmlElement
    String lastupdated

    @XmlElement
    long seasonid

    @XmlElement
    long seriesid

    @XmlElement
    long thumb_added

    @XmlElement
    long thumb_width

    @XmlElement
    long thumb_height
}

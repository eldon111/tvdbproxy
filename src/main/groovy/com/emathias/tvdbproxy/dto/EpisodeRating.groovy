package com.emathias.tvdbproxy.dto

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement

@XmlAccessorType(XmlAccessType.NONE)
class EpisodeRating {

    @XmlElement
    long id

    @XmlElement
    long UserRating

    @XmlElement
    double CommunityRating
}

package com.emathias.tvdbproxy.dto

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement(name = "Data")
@XmlAccessorType(XmlAccessType.NONE)
class RatingData {

    @XmlElement(name = "Series")
    SeriesRating[] seriesRatings;

    @XmlElement(name = "Episode")
    EpisodeRating[] episodeRatings;
}

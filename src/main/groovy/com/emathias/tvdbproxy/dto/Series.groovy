package com.emathias.tvdbproxy.dto

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement

@XmlAccessorType(XmlAccessType.NONE)
class Series {

    @XmlElement
    long id

    @XmlElement
    String Actors

    @XmlElement
    String Airs_DayOfWeek

    @XmlElement
    String Airs_Time

    @XmlElement
    String ContentRating

    @XmlElement
    String FirstAired

    @XmlElement
    String Genre

    @XmlElement
    String IMDB_ID

    @XmlElement
    String Language

    @XmlElement
    String Network

    @XmlElement
    String NetworkID

    @XmlElement
    String Overview

    @XmlElement
    float Rating

    @XmlElement
    int RatingCount

    @XmlElement
    int Runtime

    @XmlElement
    long SeriesId

    @XmlElement
    String SeriesName

    @XmlElement
    String Status

    @XmlElement
    String added

    @XmlElement
    String addedBy

    @XmlElement
    String banner

    @XmlElement
    String fanart

    @XmlElement
    String lastupdated

    @XmlElement
    String poster

    @XmlElement
    int tms_wanted_old

    @XmlElement
    String zap2it_id

    @XmlElement
    String AliasNames
}

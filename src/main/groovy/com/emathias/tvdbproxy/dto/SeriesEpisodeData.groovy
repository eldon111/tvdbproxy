package com.emathias.tvdbproxy.dto

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement(name = "Data")
@XmlAccessorType(XmlAccessType.NONE)
class SeriesEpisodeData {

    @XmlElement(name = "Series")
    Series series;

    @XmlElement(name = "Episode")
    List<Episode> episodes;
}

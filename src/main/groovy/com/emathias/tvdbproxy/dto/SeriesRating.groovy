package com.emathias.tvdbproxy.dto

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement

@XmlAccessorType(XmlAccessType.NONE)
class SeriesRating {

    @XmlElement
    long seriesid

    @XmlElement
    long UserRating

    @XmlElement
    double CommunityRating
}

package com.emathias.tvdbproxy.mapping

import com.emathias.tvdbproxy.domain.EpisodeMapping
import com.emathias.tvdbproxy.dto.Episode
import com.emathias.tvdbproxy.dto.SeriesEpisodeData
import org.springframework.util.LinkedMultiValueMap

class BasicEpisodeMapper implements EpisodeMapper {

    private final MappingFinder mappingFinder

    public BasicEpisodeMapper(EpisodeMapping[] episodeMappings) {
        this.mappingFinder = new MappingFinder(episodeMappings)
    }

    @Override
    SeriesEpisodeData mapEpisodes(SeriesEpisodeData seriesEpisodeData) {
        SeriesEpisodeData newSeriesEpisodeData = new SeriesEpisodeData()

        newSeriesEpisodeData.setEpisodes(seriesEpisodeData.episodes
                .collect { episode -> mapEpisode(episode) }
                .collectEntries(new LinkedMultiValueMap<Tuple, Episode>()) { episode ->
                    [new Tuple(episode.seasonNumber, episode.episodeNumber), episode]
                }
                .collect { entry -> combineEpisodes(entry.getValue()) })

        return newSeriesEpisodeData
    }

    private Episode mapEpisode(Episode episode) {
        Episode newEpisode = episode.clone()

        Optional<EpisodeMapping> mapping = mappingFinder.findMappingByEpisodeNumber(
                episode.seasonNumber, episode.episodeNumber)

        if (mapping.isPresent()) {
            newEpisode.setSeasonNumber(mapping.get().destSeason)
            newEpisode.setEpisodeNumber(mapping.get().destEpisode)
        }
    }

    private static Episode combineEpisodes(List<Episode> episodes) {
        // TODO: combine episodes
        return new Episode()
    }

    class MappingFinder {
        private final EpisodeMapping[] episodeMappings

        public MappingFinder(EpisodeMapping[] episodeMappings) {
            this.episodeMappings = episodeMappings
        }

        public Optional<EpisodeMapping> findMappingByEpisodeNumber(int season, int episode) {
            for (EpisodeMapping episodeMapping : episodeMappings) {
                if (episodeMapping.srcSeason == season && episodeMapping.srcEpisode == episode) {
                    return Optional.of(episodeMapping)
                }
            }
            return Optional.empty()
        }
    }
}

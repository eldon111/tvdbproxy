package com.emathias.tvdbproxy.mapping

import com.emathias.tvdbproxy.dto.SeriesEpisodeData

interface EpisodeDataSource {
    SeriesEpisodeData loadDataForSeries(long seriesId)
}
package com.emathias.tvdbproxy.mapping

import groovy.transform.Memoized

class EpisodeDataSourceFactory {

    @Memoized(maxCacheSize = 3)
    public static EpisodeDataSource getTvdbEpisodeDataSource(String apiKey, String lang) {
        return new TvdbEpisodeDataSource(apiKey, lang)
    }
}

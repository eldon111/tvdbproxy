package com.emathias.tvdbproxy.mapping

import com.emathias.tvdbproxy.dto.SeriesEpisodeData

interface EpisodeMapper {
    SeriesEpisodeData mapEpisodes(SeriesEpisodeData seriesEpisodeData)
}
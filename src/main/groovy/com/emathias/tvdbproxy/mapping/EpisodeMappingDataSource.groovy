package com.emathias.tvdbproxy.mapping

import com.emathias.tvdbproxy.domain.EpisodeMapping

interface EpisodeMappingDataSource {
    EpisodeMapping[] getMappingForSeries(long seriesId)
}

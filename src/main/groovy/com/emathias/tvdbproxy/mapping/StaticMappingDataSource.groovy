package com.emathias.tvdbproxy.mapping

import com.emathias.tvdbproxy.domain.EpisodeMapping

class StaticMappingDataSource implements EpisodeMappingDataSource {

    @Override
    EpisodeMapping[] getMappingForSeries(long seriesId) {
        if (seriesId == 78874L) {
            return getFireflyMapping()
        }
        return new EpisodeMapping[0]
    }

    private static EpisodeMapping[] getFireflyMapping() {
        return [
                new EpisodeMapping(1, 1, 1, 2),
                new EpisodeMapping(1, 2, 1, 3),
                new EpisodeMapping(1, 3, 1, 6),
                new EpisodeMapping(1, 4, 1, 7),
                new EpisodeMapping(1, 5, 1, 8),
                new EpisodeMapping(1, 6, 1, 4),
                new EpisodeMapping(1, 7, 1, 5),
                new EpisodeMapping(1, 8, 1, 9),
                new EpisodeMapping(1, 9, 1, 10),
                new EpisodeMapping(1, 10, 1, 14),
                new EpisodeMapping(1, 11, 1, 1),
                new EpisodeMapping(1, 12, 1, 13),
                new EpisodeMapping(1, 13, 1, 11),
                new EpisodeMapping(1, 14, 1, 12)
        ]
    }
}

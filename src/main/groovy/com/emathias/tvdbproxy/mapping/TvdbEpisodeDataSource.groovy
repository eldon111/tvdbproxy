package com.emathias.tvdbproxy.mapping

import com.emathias.tvdbproxy.dto.SeriesEpisodeData
import org.springframework.web.client.RestTemplate

class TvdbEpisodeDataSource implements EpisodeDataSource {

    private final String apiKey
    private final String lang

    public TvdbEpisodeDataSource(String apiKey, String lang) {
        this.apiKey = apiKey
        this.lang = lang
    }

//    @Override
//    SeriesEpisodeData loadDataForSeries(long seriesId) {
//        return null
//    }

    @Override
    SeriesEpisodeData loadDataForSeries(long seriesId) {
        RestTemplate template = new RestTemplate();

        return template.getForEntity(
                "http://thetvdb.com/api/${apiKey}/series/${seriesId}/all/${lang}.xml",
                SeriesEpisodeData).body;
    }
}

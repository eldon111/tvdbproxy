package com.emathias.tvdbproxy.service

import com.emathias.tvdbproxy.mapping.EpisodeDataSource
import com.emathias.tvdbproxy.mapping.EpisodeMapper
import com.emathias.tvdbproxy.mapping.EpisodeMappingDataSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class EpisodeMappingService {

    private final EpisodeDataSource episodeDataSource
    private final EpisodeMappingDataSource episodeMappingDataSource
    private final EpisodeMapper episodeMapper

    @Autowired
    public EpisodeMappingService(EpisodeDataSource episodeDataSource,
                                 EpisodeMappingDataSource episodeMappingDataSource,
                                 EpisodeMapper episodeMapper) {
        this.episodeDataSource = episodeDataSource
        this.episodeMappingDataSource = episodeMappingDataSource
        this.episodeMapper = episodeMapper
    }
}
